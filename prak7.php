<?php
$brush_price = 5;

echo "<table border=\"1\" align=\"center\">";
echo "<tr><th>Quantity</th>";
echo "<th>Price</th></tr>";

$counter = 10; // Inisialisasi di luar loop

do {
  echo "<tr><td>";
  echo $counter;
  echo "</td><td>";
  echo $brush_price * $counter;
  echo "</td></tr>";

  $counter += 5; // Inkremen di dalam loop
} while ($counter <= 100);

echo "</table>";

?>
