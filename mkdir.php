<?php
$nama_direktori = 'uploads'; // Ganti dengan nama direktori yang Anda inginkan

// Periksa apakah direktori sudah ada atau belum
if (!is_dir($nama_direktori)) {
    // Jika belum ada, buat direktori baru
    if (mkdir($nama_direktori, 0777, true)) {
        echo "Direktori berhasil dibuat: $nama_direktori";
    } else {
        echo "Gagal membuat direktori: $nama_direktori";
    }
} else {
    echo "Direktori sudah ada: $nama_direktori";
}
?>