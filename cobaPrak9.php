<!DOCTYPE html>
<html>
<body>
    <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        Choose a file to upload: 
        <input name="uploadedfile" type="file" /> <br>
        <input type="submit" value="Upload File" />
    </form>

<?php
        #if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $target_path = "uploads/";
        $target_path = $target_path . basename($_FILES['uploadedfile']['name']); 
        
        if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
            echo "The file ".  basename($_FILES['uploadedfile']['name']). " has been uploaded";
        } else {
            echo "There was an error uploading the file, please try again!";
        }
    #}
?>
</body>
</html>

